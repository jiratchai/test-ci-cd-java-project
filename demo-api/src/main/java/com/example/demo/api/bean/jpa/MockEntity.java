package com.example.demo.api.bean.jpa;

import javax.persistence.*;

@Entity
@Table(name = "TEMP_USER")
public class MockEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "")
    private Integer id;

    @Column(name = "")
    private String username;

    @Column(name = "FIRST_NAME")
    private String firstName;

    public MockEntity() {
    }

    public MockEntity(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
