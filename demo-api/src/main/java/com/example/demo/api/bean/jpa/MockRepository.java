package com.example.demo.api.bean.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MockRepository extends JpaRepository<MockEntity, Long> {

}