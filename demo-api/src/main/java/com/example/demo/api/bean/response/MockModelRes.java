package com.example.demo.api.bean.response;

public class MockModelRes {

    private Long id;
    private String name;

    public MockModelRes() {
    }

    public MockModelRes(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
