package com.example.demo.api.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class MockDao {

    @Autowired
    @Qualifier("jdbc")
    private JdbcTemplate jdbc;

    public String getMockData() {
        String sql = "SELECT USERNAME FROM TEMP_USER WHERE USERNAME = 'mark'";

        return this.jdbc.queryForObject(sql, new Object[]{}, String.class);
    }
}
