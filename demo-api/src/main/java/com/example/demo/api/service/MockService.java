package com.example.demo.api.service;

import com.example.demo.api.dao.MockDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MockService {
    @Autowired
    private MockDao db;

    public String getData() {
        return db.getMockData();
    }
}
