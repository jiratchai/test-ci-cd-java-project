package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;

//@Profile("oracle")
@Configuration
@EnableJpaRepositories(
        basePackages = "com.example.demo.api.bean.jpa",
        entityManagerFactoryRef = "jpaEntityManager",
        transactionManagerRef = "jpaTransactionManager"
)
public class DataSourceConfig {

    /** Config for normal jdbc */
    @Bean(name="ds")
    @Primary
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource DataSource() {
        System.out.println("##### Before call DATA Source Builder");
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "jdbc")
    public JdbcTemplate JdbcTemplate(@Qualifier("ds") DataSource dataSource) throws SQLException {
        System.out.println("##### TEST ::" + dataSource.getConnection().isClosed());
        return new JdbcTemplate(dataSource);
    }

    /** Config for JPA */
    @Autowired
    private Environment env;

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource jpaDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name="jpaEntityManager")
    public LocalContainerEntityManagerFactoryBean DashboardEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(jpaDataSource());
        em.setPackagesToScan(new String[] { "com.example.demo.api" });

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.dialect", env.getProperty("spring.jpa.dashboard.database-platform"));
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean(name = "jpaTransactionManager")
    public PlatformTransactionManager DashboardTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(DashboardEntityManager().getObject());
        return transactionManager;
    }
}
