package com.example.demo.controller;

import com.example.demo.api.service.MockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("")
public class IndexController {

    @Autowired
    private MockService mockService;

    @RequestMapping(value ="/main", method = RequestMethod.GET)
    public String index(Model model) {

        String msg = mockService.getData();

        System.out.println("msg:: " + msg);

        model.addAttribute("val", msg);

        return "main";
    }
}
