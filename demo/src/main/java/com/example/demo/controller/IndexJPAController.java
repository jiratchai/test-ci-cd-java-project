package com.example.demo.controller;

import com.example.demo.api.bean.jpa.MockEntity;
import com.example.demo.api.bean.jpa.MockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("")
public class IndexJPAController {

    @Autowired
    private MockRepository mockRepo;

    @GetMapping("/main-jpa")
    public Object main() {

        List<MockEntity> rs = mockRepo.findAll();

        return rs;
    }

}
